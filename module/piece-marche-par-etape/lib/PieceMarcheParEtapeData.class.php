<?php

class PieceMarcheParEtapeData
{

    public $nom_flux_piece = 'piece-marche';
    public $envoyer = false;
    public $id_e;
    public $id_u;
    public $libelle;
    public $numero_marche;
    public $type_marche;
    public $recurrent;
    public $numero_consultation;
    public $type_consultation;
    public $etape;
    public $soumissionnaire;
    public $montant;
    public $date_document;
}
