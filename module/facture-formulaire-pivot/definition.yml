nom: Facture Formulaire PIVOT
type: Types de dossier Facture Chorus Pro

description: |
    Le type de dossier Facture Formulaire PIVOT fonctionne de la manière suivante :
    - saisie des paramètres et données de la facture et ajout des pièces jointes
    - action "Affecter les valeurs par défaut" (le type de dossier doit être associé au connecteur de Parametrage)
    - action "Créer le fichier facture pivot" (le type de dossier doit être associé au connecteur Facture Fichier Pivot)
    - action "Intégrer au type de dossier Facture Chorus Pro"

restriction_pack:
    - pack_chorus_pro

connecteur:
    ParametrageFluxFacturePivot

formulaire:
    Destinataire:
        destinataire:
            name: Identifiant CPP du destinataire
        siret:
            name: Siret du destinataire
            requis: true
            commentaire: "De 14 caractères"
            preg_match: "#^[0-9A-Za-z_]{14}$#"
            preg_match_error: "14 caractères"
        service_destinataire:
            name: Identifiant du service destinataire
        service_destinataire_code:
            name: Code du service destinataire
        facture_numero_engagement:
            name: Numéro d'engagement
        facture_numero_marche:
            name: Numéro de marché

    Fournisseur:
        fournisseur_type_id:
            name: Type de fournisseur
            requis: true
            type: select
            value:
                1: 1- Tiers avec SIRET
                2: 2- Structure Européenne hors France
                3: 3- Structure hors UE
                4: 4- RIDET
                5: 5- Numéro TAHITI
                6: 6- Tiers en cours d'immatriculation
                7: 7- Particulier
        fournisseur:
            name: Identifiant du fournisseur
            requis: true
        fournisseur_raison_sociale:
            name: Raison sociale du fournisseur
            requis: true
            index: true
        fournisseur_code_pays:
            name: Code pays du fournisseur
            requis: true
            commentaire: "De 2 caractères"
            preg_match: "#^[0-9A-Za-z_]{2}$#"
            preg_match_error: "2 caractères"
        fournisseur_ref_bancaire_type:
            name: Référence bancaire - Type
            type: select
            value:
                IBAN: IBAN
                RIB: RIB
        fournisseur_ref_bancaire_compte:
            name: Référence bancaire - Compte
        fournisseur_ref_bancaire_etablissement:
            name: Référence bancaire - Etablissement


    Données:
        no_facture:
            name: Numéro de la facture fournisseur
            title: true
            requis: true
            commentaire: "Unique par fournisseur. De 1 à 20 caractères"
            preg_match: "#^.{1,20}$#"
            preg_match_error: "1 à 20 caractères"
        facture_type:
            name: Type de facture
            requis: true
            type: select
            value:
                380: 380- Facture
                381: 381- Avoir
        facture_cadre:
            name: Cadre de facturation
            requis: true
            type: select
            value:
                A1: A1- Dépôt par un fournisseur d'une facture
                A2: A2- Dépôt par un fournisseur d'une facture déjà payée
                A3: A3- Dépôt par un fournisseur d'un mémoire de frais de justice
                A4: A4- Dépôt par un fournisseur d'un projet de décompte mensuel
                A5: A5- Dépôt par un fournisseur d'un état d'acompte
                A6: A6- Dépôt par un fournisseur d'un état d'acompte validé
                A7: A7- Dépôt par un fournisseur d'un projet de décompte final
                A8: A8- Dépôt par un fournisseur d'un décompte général et définitif
                A9: A9- Dépôt par un sous-traitant d'une facture
                A10: A10- Dépôt par un sous-traitant d'un projet de décompte mensuel
                A12: A12- Dépôt par un cotraitant d'une facture
                A13: A13- Dépôt par un cotraitant d'un projet de décompte mensuel
                A14: A14- Dépôt par un cotraitant d'un projet de décompte final
                A15: A15- Dépôt par une MOE d'un état d'acompte
                A16: A16- Dépôt par une MOE d'un état d'acompte validé
                A17: A17- Dépôt par une MOE d'un projet de décompte général
                A18: A18- Dépôt par une MOE d'un décompte général
                A19: A19- Dépôt par une MOE d'un état d'acompte validé
                A20: A20- Dépôt par une MOE d'un décompte général
                A21: A21- Dépôt par un bénéficiaire d'une demande de remboursement de la TIC
                A22: A22- Projet de décompte général déposé par le fournisseur ou le mandataire
                A23: A23- Décompte général et définitif tacite déposé par le fournisseur ou le mandataire
                A24: A24- Dépôt d'un décompte gérénral et définitif par la maitrise d oeuvrage
                A25: A25- Décompte général et définitif déposé par la maitrise d ouvrage dans le cadre de la facturation d un marché de travaux
        date_facture:
            name: Date d'émission de la facture
            requis: true
            type: date
            index: true
        facture_date_reception:
            name: Date de reception de la facture
            requis: true
            type: date
        facture_mode_paiement_code:
            name: Code du mode de paiement
            requis: true
            type: select
            value:
                10: 10- Cash
                20: 20- Check
                30: 30- Credit transfert
                31: 31- Debit transfer
                42: 42- Payment to Bank account
                48: 48- Bank Card
                49: 49- Direct Debit
                97: 97- Clearing between partners
        facture_mode_paiement_libelle:
            name: Libellé mode de paiement
            type: select
            value:
                espece: Espèce
                cheque: Chèque
                virement: Virement
                prelevement: Prélèvement
                autre: Autre
                report: Report
        facture_devise:
            name: Devise
            requis: true
            preg_match: "#^[A-Z]{3}$#"
            preg_match_error: "3 lettres majuscules"
            commentaire: "Conforme à la norme ISO 4217"
        facture_montant_ht:
            name: Montant HT
            requis: true
        montant_ttc:
            name: Montant TTC
            requis: true
            index: true
        facture_montant_net:
            name: Net à payer
            requis: true


    Fichiers:
        facture_fichier_lignes_csv:
            name: Fichier facture lignes csv
            commentaire: Lignes de la forme ReferenceProduit;PrixUnitaire;Quantite;MontantHT;TauxTVA
            type: file
        facture_pj_01:
            name: Facture originale pdf
            commentaire: Format PDF
            requis: true
            type: file
        facture_pj_02:
            name: Pièces jointes complémentaires
            commentaire: Format PDF
            type: file
            multiple: true
        fichier_facture:
            name: Fichier CPPFacturePivot
            commentaire: Fichier xml contenant un seul bloc CPPFacturePivotUnitaire. Ce fichier peut être intégré ou créé d'après le formulaire et les fichiers joints.
            type: file

champs-affiches:
    titre
    fournisseur_raison_sociale
    date_facture
    montant_ttc
    dernier_etat
    date_dernier_etat

action:
    creation:
        name-action: Créer
        name: Créé
        rule:
            no-last-action:
            droit_id_u: 'facture-formulaire-pivot:edition'
    modification:
        name-action: Modifier
        name: En cours de rédaction
        rule:
            last-action:
                creation
                modification
                affecter-valeurs
                extraire-pivot
            role_id_e: editeur
            droit_id_u: 'facture-formulaire-pivot:edition'
    supression:
        name-action: Supprimer
        name: Supprimé
        rule:
            droit_id_u: 'facture-formulaire-pivot:edition'
        action-class: Supprimer
        warning: "Êtes-vous sûr ?"

    affecter-valeurs:
        name-action: Affecter les valeurs par défaut
        name: Affectation des valeurs par défaut
        rule:
            droit_id_u: 'facture-formulaire-pivot:edition'
            last-action:
                creation
                modification
        action-class: FactureFormulaireAffecterValeurs

    extraire-pivot:
        name-action: Renseigner d'après le fichier Pivot
        name: Renseigné d'après le fichier Pivot
        rule:
            last-action:
                modification
                affecter-valeurs
            droit_id_u: 'facture-formulaire-pivot:edition'
        editable-content:
            destinataire
            siret
            service_destinataire
            service_destinataire_code
            facture_numero_engagement
            facture_numero_marche
            fournisseur_type_id
            fournisseur
            fournisseur_raison_sociale
            fournisseur_code_pays
            fournisseur_ref_bancaire_type
            fournisseur_ref_bancaire_compte
            fournisseur_ref_bancaire_etablissement
            no_facture
            facture_type
            facture_cadre
            date_facture
            facture_date_reception
            facture_mode_paiement_code
            facture_mode_paiement_libelle
            facture_devise
            facture_montant_ht
            montant_ttc
            facture_montant_net
            facture_fichier_lignes_csv
            facture_pj_01
            facture_pj_02
            fichier_facture
        action-class: FactureFormulaireExtrairePivot

    integrer-flux-facture-cpp:
        name-action: Intégrer au type de dossier Facture Chorus Pro
        name: Intégré au type de dossier Facture Chorus Pro
        rule:
            last-action:
                creation
                modification
                affecter-valeurs
                extraire-pivot
            droit_id_u: 'facture-formulaire-pivot:edition'
            document_is_valide: true
        action-class: FactureFormulaireIntegrerCPP


