<?php

require_once __DIR__ . "/../../../connecteur-type/TdT/TdtChoiceTypologieActes.class.php";

/**
 * @deprecated PA 3.0 utiliser TdtChoiceTypologieActes à la place
 * Class ActesTypePiece
 */
class ActesTypePiece extends TdtChoiceTypologieActes
{
 /** Nothing to do */
}
