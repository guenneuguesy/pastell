<?php

require_once __DIR__ . "/../../../connecteur-type/TdT/TdtSendReponsePref.class.php";

/**
 * @deprecated PA 3.0 utiliser TdtSendReponsePref à la place
 * Class TedetisSendReponsePref
 */
class TedetisSendReponsePref extends TdtSendReponsePref
{
 /** Nothing to do */
}
