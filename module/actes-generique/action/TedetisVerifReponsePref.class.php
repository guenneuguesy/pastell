<?php

require_once __DIR__ . "/../../../connecteur-type/TdT/TdtVerifReponsePref.class.php";

/**
 * @deprecated PA 3.0 utiliser TdtVerifReponsePref à la place
 * Class TedetisVerifReponsePref
 */
class TedetisVerifReponsePref extends TdtVerifReponsePref
{
 /** Nothing to do */
}
