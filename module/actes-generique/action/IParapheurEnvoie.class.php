<?php

require_once __DIR__ . "/../../../connecteur-type/signature/SignatureEnvoie.class.php";

/**
 * Class IParapheurEnvoie
 * @deprecated PA 3.0 - utiliser la classe SignatureEnvoie à la place
 */
class IParapheurEnvoie extends SignatureEnvoie
{
 /* Nothing to do*/
}
