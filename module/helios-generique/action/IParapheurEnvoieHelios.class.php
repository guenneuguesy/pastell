<?php

require_once __DIR__ . "/../../../connecteur-type/signature/SignatureEnvoie.class.php";

/**
 * Class IparapheurEnvoieHelios
 * @deprecated  PA 3.0. Use SignatureEnvoie instead
 */
class IparapheurEnvoieHelios extends SignatureEnvoie
{
}
