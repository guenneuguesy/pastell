
Ce fichier est le fichier de base fourni par l’éditeur de Pastell.
Ce fichier doit être complété par le responsable du traitement.

# RESPONSABLE DU TRAITEMENT

A COMPLETER : LES INFORMATIONS SUR LE RESPONSABLE

# Délégué à la protection des données (DPO)

A COMPLETER : LES INFORMATIONS SUR LE DPO

# Quelles informations personnelles sont collectées ?

L’application PASTELL, collecte vos informations personnelles afin de vous proposer
et d’améliorer continuellement notre produit et service.
Vous trouverez ci-après les types d’information que l’application collecte.

## Informations qui sont renseignées dans l’application

Nous collectons et enregistrons toutes les informations que vous nous communiquez via l’application PASTELL
Lors de la création de votre compte utilisateur, les informations suivantes sont collectées :

* Nom de famille de l’utilisateur
* Prénom de l’utilisateur
* E-mail professionnel de l’utilisateur

## Informations collectées automatiquement

Lorsque vous utilisez l’application PASTELL nous utilisons seulement des « cookies » de session afin de gérer l’authentification des utilisateurs.
Nous ne recevons ni n’enregistrons aucun autre « cookies » qui permettrait notamment d’analyser votre interaction avec le contenu et les services disponibles ou tout autre « cookies » via l’application.

## Informations issues d’autres sources

Aucune information personnelle n’est récupérée auprès d’un service tiers à l’application  PASTELL.

#  Pour quelles finalités traite-t-elle mes informations personnelles ?

## Utilisation des données par l’application

Nous utilisons vos informations personnelles aux fins :
- d’horodatage des actions des utilisateurs
- de communication avec vous

## Communiquer avec vous

L’application PASTELL peut être amenée à vous envoyer des e-mails afin de vous informer sur différentes actions à mener au sein de l’application.

# Mes informations personnelles sont-elles protégées ?

##  Notre politique de sécurité des données

L’application PASTELL a été conçue dans un souci de sécurité et de protections de vos données personnelles.
Nous protégeons la sécurité de vos informations personnelles lors de leur transmission en utilisant des techniques permettant de chiffrer les informations que vous entrez avant qu’elles ne nous soient envoyées.
Il est important de vous protéger contre l’accès non autorisé à votre mot de passe et à vos ordinateurs, appareils et applications. Si vous partagez un ordinateur, vous devez vous déconnecter après chaque utilisation.

## Où sont stockées les données ?

A COMPLETER : supprimer un paragraphe suivant le cas

### Hébergement de l’application sur les serveurs de la collectivité

Dans le cas où l’application PASTELL est hébergée sur les serveurs gérés par la collectivité, les mesures de sécurité physiques, électroniques et les procédures de sauvegarde sont à mettre en place par le gestionnaire du réseau de la collectivité.

### Hébergement de l’application en mode SAAS

Dans le cas où LIBRICIEL SCOP héberge l’application PASTELL et de ce fait vos données personnelles, vos données sont hébergées chez plusieurs de nos hébergeurs uniquement sur le territoire français.
Nous maintenons des mesures de sécurité physiques, électroniques et des procédures de sauvegarde en rapport avec la collecte, la conservation et la communication d’informations personnelles de clients.
Nos procédures de sécurité peuvent nous amener à vous demander une preuve de votre identité avant de pouvoir vous communiquer vos informations personnelles.
Nos appareils disposent de fonctions de sécurité afin de les protéger contre une tentative d’accès non autorisé ou une perte d’informations.

# À quelles informations puis-je avoir accès ?

## Modification de mes données personnelles

Une fois connecté à votre espace personnel, vous avez la possibilité de modifier les informations personnelles que vous avez renseignées ou qui ont été renseignées par le responsable de votre structure en vous rendant dans « Mon compte > Préférences »
Vous pouvez aussi contacter le responsable du traitement afin qu’il modifie vos informations personnelles. Nos procédures de sécurité peuvent nous amener à vous demander une preuve de votre identité avant de pouvoir vous communiquer vos informations personnelles.

## Suppression de mes données personnelles

## Anonymisation des données personnelles

Sur demande d’un utilisateur, l’administrateur a la possibilité d’anonymiser les données personnelles le concernant.
Seules les données liées à l’horodatage ne pourront être supprimées.

# Quels choix me sont proposés ?

Lors de l’utilisation des services, vous pouvez également paramétrer certaines options :
Si vous ne souhaitez pas recevoir d’e-mails de notification de la part de l’application PASTELL, veuillez modifier vos préférences de notification.
En application de la loi en vigueur, vous disposez d’un droit d’accès, de modification, de rectification et de suppression des informations vous concernant, ainsi que du droit de demander la portabilité de ces informations.

# Contacts, notices et révisions

Si vous avez des questions concernant la protection des informations personnelles par l’application PASTELL, merci d‘envoyer un message détaillé par e-mail.
De plus, le délégué à la protection des données (DPO) pour les responsables de traitements ci-avant mentionnés peut être contacté à l’adresse suivante : ADRESSE DU DPO.
Vous pourrez déposer une réclamation auprès de notre principale autorité de surveillance, la CNIL (https://www.cnil.fr/).

IMPORTANT : LIBRICIEL SCOP garantit le respect de cette politique de confidentialité dans le cadre de l’exécution du contrat de maintenance avec Libriciel SCOP ou l’un de ses partenaires et sans modification du code source de l’application.