<?php

?>

<div class="alert-info alert">
<p>Pour les clients <a href="https://www.libriciel.fr" target="_blank">Libriciel</a>, la documentation est disponible
    en cliquant sur le lien ci-dessous :
</p>
<a href="https://otrs.libriciel.fr/otrs/customer.pl?Action=CustomerFAQExplorer;CategoryID=9" target="_blank">
    Accéder à la documentation
</a>
</div>