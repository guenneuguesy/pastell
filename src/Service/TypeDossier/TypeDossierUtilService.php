<?php

namespace Pastell\Service\TypeDossier;

class TypeDossierUtilService
{
    public const TYPE_DOSSIER_CLASSEMENT_DEFAULT = "Types de dossier personnalisés";
    public const ID_T = 'id_t';
    public const ID_TYPE_DOSSIER = 'id_type_dossier';
    public const ORIG_ID_TYPE_DOSSIER = 'orig_id_type_dossier';
    public const RAW_DATA = 'raw_data';
    public const TIMESTAMP = 'timestamp';
    public const PASTELL_VERSION = 'pastell-version';
}
