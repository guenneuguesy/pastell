<?php

class TypeDossierFormulaireElementProperties
{


    public $element_id;
    public $name;
    public $type;
    public $commentaire;
    public $requis;
    public $champs_affiches;
    public $champs_recherche_avancee;
    public $titre;
    public $select_value;
    public $preg_match;
    public $preg_match_error;
    public $content_type;
}
