<?php

class Acte
{
    public $date;
    public $numero;
    public $codeNature;
    public $classification;
    public $classificationDate;
    public $documentPapier;
    public $object;
    public $idActe;
    public $acte;
    /**
     * @var array $annexes
     */
    public $annexes;
}
