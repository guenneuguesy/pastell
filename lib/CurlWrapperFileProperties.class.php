<?php

class CurlWrapperFileProperties
{
    public $field;
    public $filename;
    public $filepath;
    public $contentType;
    public $contentTransferEncoding;
}
