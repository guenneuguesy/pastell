# Pré-requis à l'installation de Pastell.

Pastell est prévu pour fonctionner sur un environnement LAMP (Linux Apache MySQL PHP).
Il peut sans doute fonctionner sur d'autre OS (MS Windows, Mac OS) et d'autres serveurs web (nginx, lighthttpd). 
Cela n'a toutefois pas été testé.


## Système d'exploitation
Pastell est développé et testé sur le système Ubuntu GNU/Linux.
La version minimale recommandée pour Pastell est  :
		
		Ubuntu GNU/Linux 16.04 LTS

Dans cette version, les paquet apache2, php7 et mysql-server sont suffisant pour 
l'environnement LAMP de Pastell.

## Autres dépendances 

Sur la page **Environnement système** de Pastell, il est possible de voir les 
autres dépendances de Pastell et si celle-ci sont disponible sur l'environnement courant.

L'ensemble des dépendances peut également être déduite du fichier Dockerfile.


## Navigateur compatible 
Pastell est développé dans le respect des standard du web et de l'accessibilité.

Toutefois, Pastell a été développer et tester principalement sur Google Chrome et Mozilla Firefox.





